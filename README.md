# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : [xxxx]
* Key functions (add/delete)
    1. [xxxx]
    
* Other functions (add/delete)
    1. [xxxx]
    2. [xxxx]
    3. [xxxx]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|N|
|Firebase Page|5%|N|
|Database|15%|N|
|RWD|15%|N|
|Topic Key Function|15%|N|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|N|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|

## Website Detail Description

# 作品網址：[xxxx]

# Components Description : 
1. [xxxx] : [xxxx]
2. [xxxx] : [xxxx]
3. [xxxx] : [xxxx]
...

# Other Functions Description(1~10%) : 
1. [xxxx] : [xxxx]
2. [xxxx] : [xxxx]
3. [xxxx] : [xxxx]
...

## Security Report (Optional)
